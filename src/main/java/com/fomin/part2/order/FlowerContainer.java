package com.fomin.part2.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FlowerContainer {
    @Autowired
    private List<Flower> flowers;

    public List<Flower> getFlowers() {
        return flowers;
    }
}
