package com.fomin.part1.validation;

public interface BeanValidator {
    boolean validate();
}
