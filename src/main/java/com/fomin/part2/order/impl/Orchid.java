package com.fomin.part2.order.impl;

import com.fomin.part2.order.Flower;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("orchid")
public class Orchid implements Flower {
    private final String name = "Orchid";

    @Override
    public String getName() {
        return name;
    }
}
