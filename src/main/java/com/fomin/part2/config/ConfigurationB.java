package com.fomin.part2.config;

import com.fomin.part2.beans2.TulipFlower;
import com.fomin.part2.beans2.RoseFlower;
import com.fomin.part2.beans3.BeanD;
import com.fomin.part2.beans3.BeanF;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "com.fomin.part2.beans2",
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.REGEX, pattern = "Flower$"),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                        BeanD.class, BeanF.class
                })
        })
})
public class ConfigurationB {
    @Bean
    public BeanD beanD() {
        return new BeanD();
    }

    @Bean
    public BeanF beanF() {
        return new BeanF();
    }

    @Bean
    public TulipFlower tulipFlower() {
        return new TulipFlower();
    }

    @Bean
    public RoseFlower roseFlower() {
        return new RoseFlower();
    }
}
