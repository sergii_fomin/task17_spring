package com.fomin.part2.config;

import com.fomin.part2.order.FlowerContainer;
import com.fomin.part2.order.impl.Orchid;
import com.fomin.part2.order.impl.Camomile;
import com.fomin.part2.order.impl.Rose;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class FlowerConfiguration {

    @Bean
    public Orchid bluebell() {
        return new Orchid();
    }

    @Bean
    public Camomile poppy() {
        return new Camomile();
    }

    @Bean
    public Rose rose() {
        return new Rose();
    }

    @Bean
    public FlowerContainer flowerContainer() {
        return new FlowerContainer();
    }
}
