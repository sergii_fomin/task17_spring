package com.fomin.part1.bean;

import com.fomin.part1.validation.BeanValidator;
import org.springframework.stereotype.Component;

@Component
public class BeanF implements BeanValidator {
    private String name;

    private int value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
