package com.fomin.part2.order.impl;

import com.fomin.part2.order.Flower;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("сamomile")
public class Camomile implements Flower {
    private final String name = "Camomile";

    @Override
    public String getName() {
        return name;
    }


}
