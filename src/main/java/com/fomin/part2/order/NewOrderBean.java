package com.fomin.part2.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class NewOrderBean {
    @Autowired
    @Qualifier("orchid")
    private Flower orchid;

    @Autowired
    @Qualifier("сamomile")
    private Flower сamomile;

    @Autowired
    private Flower rose;
}
