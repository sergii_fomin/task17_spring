package com.fomin.part2;

import com.fomin.part2.config.ConfigurationA;
import com.fomin.part2.order.Flower;
import com.fomin.part2.order.FlowerContainer;
import com.fomin.part2.other.OtherBeanA;
import com.fomin.part2.other.OtherBeanB;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ConfigurationA.class);
        var flowerWrapper = context.getBean(FlowerContainer.class);
        for (Flower flower : flowerWrapper.getFlowers()) {
            System.out.println("Flower - " + flower.getName());
        }

        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(context.getBean("otherBeanC"));
        System.out.println(context.getBean("otherBeanC"));
    }
}
