package com.fomin.part2.beans1;

import com.fomin.part2.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    @Qualifier("OtherBeanC")
    private OtherBeanC otherBeanC;

    public BeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }
}
