package com.fomin.part2.beans1;

import com.fomin.part2.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
    @Autowired
    private OtherBeanA otherBeanA;
}
