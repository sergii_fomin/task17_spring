package com.fomin.part2.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("OtherBeanC")
public class OtherBeanC {
    private String name = "Other bean C";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
