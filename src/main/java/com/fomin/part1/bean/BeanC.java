package com.fomin.part1.bean;

import com.fomin.part1.validation.BeanValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanC implements BeanValidator {
    @Value("${beanC.name}")
    private String name;

    @Value("${beanC.value}")
    private int value;

    public void init() {
        System.out.println("Bean C init method");
    }

    private void destroy() {
        System.out.println("Bean C destroy method");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }

}
