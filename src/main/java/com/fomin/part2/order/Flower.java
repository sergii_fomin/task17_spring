package com.fomin.part2.order;

public interface Flower {
    String getName();
}
