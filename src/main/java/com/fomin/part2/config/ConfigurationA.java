package com.fomin.part2.config;

import com.fomin.part2.beans1.BeanA;
import com.fomin.part2.beans1.BeanB;
import com.fomin.part2.beans1.BeanC;
import com.fomin.part2.beans2.Animal;
import com.fomin.part2.beans3.BeanE;
import com.fomin.part2.other.OtherBeanA;
import com.fomin.part2.other.OtherBeanB;
import com.fomin.part2.other.OtherBeanC;
import org.springframework.context.annotation.*;

@Configuration
@Import(FlowerConfiguration.class)
@ComponentScan("com.fomin.part2.beans1")
public class ConfigurationA {
    @Bean
    public BeanA beanA() {
        return new BeanA();
    }

    @Bean
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean
    public BeanC beanC() {
        return new BeanC(otherBeanC());
    }

    @Bean
    public Animal animal() {
        return new Animal();
    }

    @Bean
    public BeanE beanE() {
        return new BeanE();
    }

    @Bean
    @Scope("singleton")
    public OtherBeanA otherBeanA() {
        return new OtherBeanA();
    }

    @Bean
    @Scope("prototype")
    public OtherBeanB otherBeanB() {
        return new OtherBeanB();
    }

    @Bean
    @Scope("singleton")
    public OtherBeanC otherBeanC() {
        return new OtherBeanC();
    }
}
