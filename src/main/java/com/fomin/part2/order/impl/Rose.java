package com.fomin.part2.order.impl;

import com.fomin.part2.order.Flower;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Rose implements Flower {
    private final String name = "Rose";

    @Override
    public String getName() {
        return name;
    }
}
